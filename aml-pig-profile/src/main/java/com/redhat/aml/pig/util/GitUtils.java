package com.redhat.aml.pig.util;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.CredentialsProvider;

import com.google.common.io.Files;

public class GitUtils {
	private final File location;
	
	public GitUtils() {
		location = getLocation();
	}
	
	public void cloneRepo(String source, String password)
			throws GitAPIException, IOException {
		CredentialsProvider credentialsProvider = new GitCredentialsProvider(password);
		location.delete();
		Git result = null;
		try {
			result = Git.cloneRepository()
					.setCredentialsProvider(credentialsProvider).setURI(source)
					.setDirectory(location).setBare(false).call();
		} finally {
			if (result != null) {
				result.close();
			}
		}
	}
	
	public void addCommitAndPush(String password) throws GitAPIException, IOException {
		addAllChangesAndCommit("Update from " + new Date().toString());
		pushAllBranchesToOrigin(password);
	}
	
	public void addAllChangesAndCommit(String commitMsg)
			throws GitAPIException, IOException {
		add(".", false);
		add(".", true);
		commit(commitMsg);
	}

	public void commit( String commitMsg) throws GitAPIException,
			IOException {
		Git repo = null;
		try {
			repo = Git.open(location);
			repo.commit().setMessage(commitMsg).call();
		} finally {
			if (repo != null) {
				repo.close();
			}
		}
	}

	public void add(String filePattern, boolean update)
			throws GitAPIException, IOException {
		Git repo = null;
		try {
			repo = Git.open(location);
			repo.add().addFilepattern(filePattern).setUpdate(update).call();
		} finally {
			if (repo != null) {
				repo.close();
			}
		}
	}

	public void pushAllBranchesToOrigin(String password)
			throws GitAPIException, IOException {
		CredentialsProvider credentialsProvider = new GitCredentialsProvider(password);
		Git repo = null;
		try {
			repo = Git.open(location);
			repo.push().setPushAll()
					.setCredentialsProvider(credentialsProvider).call();
		} finally {
			if (repo != null) {
				repo.close();
			}
		}
	}
	
	public void newDecisionTable(String project, File newFile) {
		File table = new File(location.getPath() + "/"+project+"/src/main/resources/com/redhat/antimoneylaundering/CustomerProfileGDT.gdst");
		table.delete();
		try {
			Files.copy(newFile, table);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
    private File getLocation() {
        String tempDir = System.getProperty("java.io.tmpdir") + "/" + "aml";
        return new File(tempDir, "aml-" + UUID.randomUUID().toString());
    }

}
