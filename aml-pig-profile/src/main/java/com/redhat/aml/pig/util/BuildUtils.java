package com.redhat.aml.pig.util;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.redhat.aml.pig.model.BuildConfig;

public class BuildUtils {
	public static void install(String repository, String project, String hostname,
			String username, String password) {
		BuildConfig buildConfig = new BuildConfig();
		String url = "/business-central/rest/repositories/" + repository
				+ "/projects/" + project + "/maven/install/";

		ObjectMapper mapper = new ObjectMapper();
		HttpEntity httpEntity = null;
		try {
			httpEntity = new StringEntity(
					mapper.writeValueAsString(buildConfig),
					ContentType.create("application/json"));
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		HttpHost target = new HttpHost(hostname, 8080);
		CredentialsProvider credsProvider = new BasicCredentialsProvider();
		credsProvider.setCredentials(
				new AuthScope(target.getHostName(), target.getPort()),
				new UsernamePasswordCredentials(username, password));
		CloseableHttpClient httpclient = HttpClients.custom()
				.setDefaultCredentialsProvider(credsProvider).build();
		HttpPost httpPost = new HttpPost(url);
		httpPost.setEntity(httpEntity);
		AuthCache authCache = new BasicAuthCache();
		BasicScheme basicAuth = new BasicScheme();
		authCache.put(target, basicAuth);
		HttpClientContext localContext = HttpClientContext.create();
		localContext.setAuthCache(authCache);

		try {
			System.out.println(httpPost.getMethod());
			System.out.println(httpPost.getURI());
			CloseableHttpResponse response = httpclient.execute(target,
					httpPost);
			System.out.println(response);
			response.close();
		} catch (ClientProtocolException e) {
			System.out.println("Protocol Error" + e);
			throw new RuntimeException(e);
		} catch (IOException e) {
			System.out.println("IOException while getting response" + e);
			throw new RuntimeException(e);
		}

	}
}
