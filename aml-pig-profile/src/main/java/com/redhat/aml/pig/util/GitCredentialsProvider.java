package com.redhat.aml.pig.util;

import org.apache.log4j.Logger;
import org.eclipse.jgit.errors.UnsupportedCredentialItem;
import org.eclipse.jgit.transport.CredentialItem;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.URIish;

public class GitCredentialsProvider extends CredentialsProvider {
	private static final Logger LOG = Logger.getLogger(GitCredentialsProvider.class);

	private static final String PASSWORD_PROMPT = "Password:";
	private static final String NEW_HOST_PROMPT = "can't be established.";
	private String password;

	public GitCredentialsProvider(String password) {
		this.password = password;
	}

	@Override
	public boolean isInteractive() {
		return false;
	}

	@Override
	public boolean supports(CredentialItem... items) {
		for (CredentialItem item : items) {
			LOG.info(item);
			if (!(item instanceof CredentialItem.StringType)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean get(URIish uri, CredentialItem... items)
			throws UnsupportedCredentialItem {
		LOG.info("URI : " + uri);

		
        for (CredentialItem item : items) {
        	LOG.info(item.getPromptText() + item);
        	if (item instanceof CredentialItem.StringType) {
        		if (item.getPromptText().contains(PASSWORD_PROMPT)){
        			((CredentialItem.StringType) item).setValue(password);
        			return true;
        		} else {
        			LOG.error(item.getPromptText());
        		}
        	}
        	
        	if (item instanceof CredentialItem.YesNoType) {
        		if (item.getPromptText().contains(NEW_HOST_PROMPT)) {
        			((CredentialItem.YesNoType) item).setValue(true);
        			return true;
        		} else {
        			LOG.error(item.getPromptText());
        		}
        	}
        }
        return false;
	}
}
