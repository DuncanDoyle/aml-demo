package com.redhat.aml.table.model;

public class SetFieldActionColumn<T> extends Column {
	private static final String elementName = "set-field-col52";
	private DataType<T> dataType;
	private T defaultValue;
	private String header;
	private String boundName;
	private String factField;
	private String type;
	private Boolean update;
	public SetFieldActionColumn(DataType<T> dataType, T defaultValue,
			String header, String boundName, String factField, String type,
			Boolean update) {
		super();
		this.dataType = dataType;
		this.defaultValue = defaultValue;
		this.header = header;
		this.boundName = boundName;
		this.factField = factField;
		this.type = type;
		this.update = update;
	}
	public DataType<T> getDataType() {
		return dataType;
	}
	public T getDefaultValue() {
		return defaultValue;
	}
	public String getHeader() {
		return header;
	}
	public String getBoundName() {
		return boundName;
	}
	public String getFactField() {
		return factField;
	}
	public String getType() {
		return type;
	}
	public Boolean getUpdate() {
		return update;
	}
	public String getElementname() {
		return elementName;
	}
}
