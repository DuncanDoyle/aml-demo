package com.redhat.aml.table;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

import com.redhat.aml.model.CustomerProfile;
import com.redhat.aml.table.model.AttributeColumn;
import com.redhat.aml.table.model.AuditLogFilter;
import com.redhat.aml.table.model.Column;
import com.redhat.aml.table.model.ConditionColumn;
import com.redhat.aml.table.model.DataType;
import com.redhat.aml.table.model.DecisionTable;
import com.redhat.aml.table.model.EntryType;
import com.redhat.aml.table.model.Pattern;
import com.redhat.aml.table.model.SetFieldActionColumn;

public class GuidedDecisionTableWriter {
	private final File file;
	private final PrintWriter writer;
	private final DecisionTable decisionTable;
	private int index = 0;

	public GuidedDecisionTableWriter(File file, DecisionTable decisionTable) {
		this.file = file;
		this.file.delete();
		PrintWriter pw;
		try {
			pw = new PrintWriter(file);
			this.writer = pw;
			this.decisionTable = decisionTable;
			this.writeHeader(decisionTable);
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException("Uh Oh! File not found", e);
		}
	}
	
	public void writeCustomers(List<CustomerProfile> customerProfiles) {
		for (CustomerProfile customerProfile : customerProfiles) {
			this.writeCustomer(customerProfile);
		}
	}
	
	public void writeHeader(DecisionTable decisionTable) throws FileNotFoundException {
		writer.println("<decision-table52>");
		writer.println("  <tableName>"+decisionTable.getTableName()+"</tableName>");
		writer.println("  <rowNumberCol>");
		writer.println("    <hideColumn>"+decisionTable.getHideRowNumberColumn()+"</hideColumn>");
		writer.println("    <width>-1</width>");
		writer.println("  </rowNumberCol>");
		writer.println("  <descriptionCol>");
		writer.println("    <hideColumn>"+decisionTable.getHideDescriptionColumn()+"</hideColumn>");
		writer.println("    <width>-1</width>");
		writer.println("  </descriptionCol>");
		writer.println("  <metadataCols/>");
		
		this.writeAttributeColumns();
		this.writePatterns();
		
		
		this.writeActionColumns();
		this.writeAuditLogger();
		this.writeImports();
		writer.println("  <packageName>"+decisionTable.getPackageName()+"</packageName>");
		writer.println("  <tableFormat>"+decisionTable.getTableFormat()+"</tableFormat>");
		writer.println("  <data>");


	}
	
	private void writePatterns() {
		writer.println("  <conditionPatterns>");
		for (Pattern pattern : decisionTable.getPatterns()) {
			this.writePattern(pattern);
		}
		writer.println("  </conditionPatterns>");
	}

	private void writePattern(Pattern pattern) {
		writer.println("    <Pattern52>");
		writer.println("      <factType>"+pattern.getFactType()+"</factType>");
		writer.println("      <boundName>"+pattern.getBoundName()+"</boundName>");
		writer.println("      <isNegated>"+pattern.getIsNegated()+"</isNegated>");
		this.writeConditionColumns(pattern.getConditionColumns());
		
		writer.println("      <window>");
		writer.println("        <parameters/>");
		writer.println("      </window>");
		writer.println("    </Pattern52>");
	}

	private void writeConditionColumns(List<ConditionColumn<?>> columns) {
		writer.println("      <conditions>");
		for (ConditionColumn<?> condition : columns) {
			this.writeConditionColumn(condition);
		}
		writer.println("      </conditions>");
	}

	private void writeConditionColumn(ConditionColumn<?> condition) {
		writer.println("        <condition-column52>");
		this.writeTypedDefaultValue(condition.getDataType(), condition.getDefaultValue());
		writer.println("          <hideColumn>"+condition.getHideColumn()+"</hideColumn>");
		writer.println("          <width>"+condition.getWidth()+"</width>");
		writer.println("          <header>"+condition.getHeader()+"</header>");
		writer.println("          <constraintValueType>"+condition.getConstraintValueType()+"</constraintValueType>");
		writer.println("          <factField>"+condition.getFactField()+"</factField>");
		writer.println("          <fieldType>"+condition.getFieldType()+"</fieldType>");
		writer.println("          <operator>"+condition.getOperator()+"</operator>");
		writer.println("          <parameters/>");
		writer.println("        </condition-column52>");
	}

	private void writeActionColumns() {
		writer.println("  <actionCols>");
		for (Column column : decisionTable.getActionColumns()) {
			this.writeActionColumn(column);
		}
		writer.println("  </actionCols>");
	}

	private void writeActionColumn(Column column) {
		//TODO: Add support for other types of action columns
		if (!(column instanceof SetFieldActionColumn)){
			throw new IllegalArgumentException("Only SetFieldActionColumn type is supported, not " + column.getClass());
		}
		SetFieldActionColumn<Double> actionColumn = (SetFieldActionColumn<Double>) column;
		writer.println("    <"+actionColumn.getElementname()+">");
		this.writeTypedDefaultValue(actionColumn.getDataType(), actionColumn.getDefaultValue());
		writer.println("      <hideColumn>"+actionColumn.getHideColumn()+"</hideColumn>");
		writer.println("      <width>"+actionColumn.getWidth()+"</width>");
		writer.println("      <header>"+actionColumn.getHeader()+"</header>");
		writer.println("      <boundName>"+actionColumn.getBoundName()+"</boundName>");
		writer.println("      <factField>"+actionColumn.getFactField()+"</factField>");
		writer.println("      <type>"+actionColumn.getType()+"</type>");
		writer.println("      <update>"+actionColumn.getUpdate()+"</update>");
		writer.println("    </"+actionColumn.getElementname()+">");
	}

	private void writeAuditLogger() {
		writer.println("  <auditLog>");
		this.writeAuditLogger(decisionTable.getAuditLogger());
		writer.println("  </auditLog>");
	}
	
	private void writeAuditLogger(AuditLogFilter auditLogger) {
		writer.println("    <filter class=\""+auditLogger.getClassName()+"\">");
		writer.println("      <acceptedTypes>");
		for (EntryType entryType : auditLogger.getTypes()) {
			this.writeEntryType(entryType);
		}
		writer.println("      </acceptedTypes>");
		writer.println("    </filter>");
		writer.println("    <entries/>");
		
	}

	private void writeEntryType(EntryType entryType) {
		writer.println("        <entry>");
		writer.println("          <string>"+entryType.getName()+"</string>");
		writer.println("          <boolean>"+entryType.getEnabled()+"</boolean>");
		writer.println("        </entry>");
	}

	public void writeAttributeColumns() {
		writer.println("  <attributeCols>");
		for (AttributeColumn<?> attributeColumn : decisionTable.getAttributeColumns()) {
			this.writeAttributeColumn(attributeColumn);
		}
		writer.println("  </attributeCols>");
	}
	
	public void writeAttributeColumn(AttributeColumn<?> attributeColumn) {
		writer.println("    <attribute-column52>");
		//TODO refactor to use this.writeTypedDefaultValue
		writer.println("      <typedDefaultValue>");
		DataType<?> type = attributeColumn.getType();
		writer.println(String.format(type.getContainer(), attributeColumn.getDefaultValue()));
		writer.println("        <dataType>"+type.getType()+"</dataType>");
		writer.println("        <isOtherwise>"+type.getIsOtherwise()+"</isOtherwise>");
		writer.println("      </typedDefaultValue>");
		writer.println("      <hideColumn>"+attributeColumn.getHideColumn()+"</hideColumn>");
		writer.println("      <width>"+attributeColumn.getWidth()+"</width>");
		writer.println("      <attribute>"+attributeColumn.getAttributeName()+"</attribute>");
		writer.println("      <reverseOrder>"+attributeColumn.getReverseOrder()+"</reverseOrder>");
		writer.println("      <useRowNumber>"+attributeColumn.getUseRowNumber()+"</useRowNumber>");
		writer.println("    </attribute-column52>");
	}
	
	public void writeCustomer(CustomerProfile customerProfile){
			index++;
			writer.println("    <list>");
			//index
			this.writeValue(index);
			//description
			this.writeValue("");
			//ruleflow
			this.writeValue("CustomerProfileGDT");
			//no-loop
			this.writeValue(true);
			//zip code
			this.writeValue(customerProfile.getZipCode());
			//occupation
			this.writeValue(customerProfile.getOccupation());
			//minimum
//			this.writeValue(Math.round(customerProfile.getMinimum()));
			//maximum
			this.writeValue(Math.round(customerProfile.getMaximum()));
			//transaction type
			this.writeValue(customerProfile.getTransactionType());
			//transaction score
			this.writeValue(10.0);
			writer.println("    </list>");
	}
	
	private void writeValue(Object value){
		if (value instanceof String) {
			this.writeValue(DataType.STRING, (String) value);
		} else if (value instanceof Double) {
			this.writeValue(DataType.DOUBLE, (Double) value);
		} else if (value instanceof Boolean) {
			this.writeValue(DataType.BOOLEAN, (Boolean) value);
		} else if (value instanceof Long) {
			this.writeValue(DataType.LONG, (Long) value);
		} else if (value instanceof Integer) {
			this.writeValue(DataType.INTEGER, (Integer) value);
		}
	}
	
	private void writeTypedDefaultValue(DataType<?> type, Object value) {
		writer.println("      <typedDefaultValue>");
		if (value != null) {
			writer.println(String.format(type.getContainer(), value));
		}
		writer.println("        <dataType>"+type.getType()+"</dataType>");
		writer.println("        <isOtherwise>"+type.getIsOtherwise()+"</isOtherwise>");
		writer.println("      </typedDefaultValue>");
	}

	
	private <T> void writeValue(DataType<T> dataType, T value) {
		writer.println("      <value>");
		writer.println(String.format(dataType.getContainer(), value));
		writer.println("        <dataType>"+dataType.getType()+"</dataType>");
		writer.println("        <isOtherwise>"+dataType.getIsOtherwise()+"</isOtherwise>");
		writer.println("      </value>");
	}
	
	private void writeImports() {
		writer.println("  <imports>");
		writer.println("    <imports>");
		for (String importName : decisionTable.getImports()) {
			this.writeImport(importName);
		}
		writer.println("    </imports>");
		writer.println("  </imports>");
	}
	
	private void writeImport(String importFQCN) {
		writer.println("      <org.drools.workbench.models.datamodel.imports.Import>");
		writer.println("        <type>"+importFQCN+"</type>");
		writer.println("      </org.drools.workbench.models.datamodel.imports.Import>");
	}
	
	public void writeFooter(){
		writer.println("  </data>");
		writer.println("</decision-table52>");
	}
	
	public void close() {
		writeFooter();
		writer.close();
	}
	
}
