package com.redhat.aml.table.model;

public class AttributeColumn<T> {
	private Boolean hideColumn = false;
	private Boolean reverseOrder = false;
	private Boolean useRowNumber = false;
	private Integer width = -1;
	
	private DataType<T> type;
	private T defaultValue;
	private String attributeName;
	
	public AttributeColumn(DataType<T> type, T defaultValue, String attributeName) {
		this.type = type;
		this.defaultValue = defaultValue;
		this.attributeName = attributeName;
	}

	public Boolean getHideColumn() {
		return hideColumn;
	}

	public void setHideColumn(Boolean hideColumn) {
		this.hideColumn = hideColumn;
	}

	public Boolean getReverseOrder() {
		return reverseOrder;
	}

	public void setReverseOrder(Boolean reverseOrder) {
		this.reverseOrder = reverseOrder;
	}

	public Boolean getUseRowNumber() {
		return useRowNumber;
	}

	public void setUseRowNumber(Boolean useRowNumber) {
		this.useRowNumber = useRowNumber;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public DataType<T> getType() {
		return type;
	}

	public void setType(DataType<T> type) {
		this.type = type;
	}

	public T getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(T defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
}
