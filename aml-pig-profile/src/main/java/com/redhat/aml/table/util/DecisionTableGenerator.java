package com.redhat.aml.table.util;

import com.redhat.aml.table.model.AttributeColumn;
import com.redhat.aml.table.model.ConditionColumn;
import com.redhat.aml.table.model.DataType;
import com.redhat.aml.table.model.DecisionTable;
import com.redhat.aml.table.model.Pattern;
import com.redhat.aml.table.model.SetFieldActionColumn;

public class DecisionTableGenerator {
	public static DecisionTable getCustomerProfileDecisionTable() {
		DecisionTable decisionTable = new DecisionTable("com.redhat.antimoneylaundering", "CustomerProfileGDT");
		
		Pattern account = new Pattern("Account", "act");
		account
			.addConditionColumns(new ConditionColumn<String>(DataType.STRING, null, "Zip Code", "1", "zipCode", "String", "=="))
			.addConditionColumns(new ConditionColumn<String>(DataType.STRING, null, "Occupation", "1", "occupation", "String", "=="))
			;
		
		Pattern transaction = new Pattern("Transaction", "trs");
		transaction
//			.addConditionColumns(new ConditionColumn<Long>(DataType.LONG, null, "Avg - Std Dev", "1", "amount", "Long", "&lt;"))
			.addConditionColumns(new ConditionColumn<Long>(DataType.LONG, null, "Avg + Std Dev", "1", "amount", "Long", "&gt;"))
			.addConditionColumns(new ConditionColumn<String>(DataType.STRING, null, "Transaction Type", "1", "transactiontype", "String", "=="))
			;
		
		decisionTable
			.addAttributeColumn(new AttributeColumn<String>(DataType.STRING,"CustomerProfileGDT", "ruleflow-group"))
			.addAttributeColumn(new AttributeColumn<Boolean>(DataType.BOOLEAN,true, "no-loop"))
			.addPattern(account)
			.addPattern(transaction)
			.addActionColumn(new SetFieldActionColumn<Double>(DataType.DOUBLE, null, "TransactionScore", "trs", "transactionScore", "Double", false))
			//Default audit logger
			.addImport("com.redhat.aml.model.Transaction")
			.addImport("com.redhat.aml.model.Account")
			//Default table format (EXTENDED_ENTRY)
			;
		
		return decisionTable;
	}
}
