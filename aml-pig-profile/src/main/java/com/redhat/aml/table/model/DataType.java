package com.redhat.aml.table.model;

public class DataType<T> {
	public static DataType<Integer> INTEGER = new DataType<Integer>("NUMERIC_INTEGER", "<valueNumeric class=\"int\">%s</valueNumeric>");
	public static DataType<Boolean> BOOLEAN = new DataType<Boolean>("BOOLEAN", "<valueBoolean>%s</valueBoolean>");
	public static DataType<String> STRING = new DataType<String>("STRING", "<valueString>%s</valueString>");
	public static DataType<Long> LONG = new DataType<Long>("NUMERIC_LONG", "<valueNumeric class=\"long\">%s</valueNumeric>");
	public static DataType<Double> DOUBLE = new DataType<Double>("NUMERIC_DOUBLE", "<valueNumeric class=\"double\">%s</valueNumeric>");
	
	private Boolean isOtherwise = false;
	
	private String type;
	private String container;
	
	private DataType(String type, String container) {
		this.type = type;
		this.container = container;
	}

	public String getType() {
		return type;
	}

	public String getContainer() {
		return container;
	}

	public Boolean getIsOtherwise() {
		return isOtherwise;
	}
}
