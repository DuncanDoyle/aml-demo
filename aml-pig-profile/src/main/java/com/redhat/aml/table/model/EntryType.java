package com.redhat.aml.table.model;

public class EntryType {
	private String name;
	private Boolean enabled = false;
	
	public EntryType(String name) {
		this.name = name;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	public String getName() {
		return this.name;
	}
	
}
