package com.redhat.aml.table.model;

import java.util.ArrayList;
import java.util.List;

public class AuditLogFilter {
	private String className;
	private List<EntryType> types;
	
	public AuditLogFilter(String className) {
		this.className = className;
		types = new ArrayList<EntryType>(5);
		types.add(new EntryType("INSERT_ROW"));
		types.add(new EntryType("INSERT_COLUMN"));
		types.add(new EntryType("DELETE_ROW"));
		types.add(new EntryType("DELETE_COLUMN"));
		types.add(new EntryType("UPDATE_COLUMN"));
	}

	public String getClassName() {
		return className;
	}

	public List<EntryType> getTypes() {
		return types;
	}
	
	
}
