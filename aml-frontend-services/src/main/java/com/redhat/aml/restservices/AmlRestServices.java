package com.redhat.aml.restservices;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class AmlRestServices extends Application {

}
