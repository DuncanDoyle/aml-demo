package com.redhat.aml.restservices;

import java.sql.Date;
import java.util.UUID;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.redhat.aml.model.service.AccountService;
import com.redhat.aml.model.service.TransactionService;

@Path("test")
@Stateless
public class TestEndpoint {

	@Inject
	AccountService accountService;

	@Inject
	TransactionService transactionService;

	@POST
	@Path("create/account/{count}")
	public void createTestAccounts(@PathParam("count") int count) {
		for (int i = 0; i < count; i++) {
			accountService.createAccount(UUID.randomUUID().toString(),
					"Savings", 3, "New York", "Elmo", "Monster", "s",
					"Student", "m", "NY", "123 Sesame Street", "10001", "7045551234");
		}
	}

	@POST
	@Path("create/transaction/{count}")
	public void createTestTransactions(@PathParam("count") int count) {
		for (int i = 0; i < count; i++) {
			transactionService.createTransaction("Elmo", "Monster", UUID
					.randomUUID().toString(), Long.valueOf(1000), "Deposit",
					"Hooper's Store", "NY", "192.168.0.1", "", "US", "NY", Date
							.valueOf("2015-08-05"), "");
		}
	}

}
