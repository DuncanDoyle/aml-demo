package com.redhat.aml.restservices;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import com.redhat.aml.model.Account;
import com.redhat.aml.model.AccountDB;
import com.redhat.aml.model.Transaction;
import com.redhat.aml.model.TransactionDB;
import com.redhat.aml.model.service.AccountService;
import com.redhat.aml.model.service.TransactionService;
import com.redhat.aml.model.transform.AccountTransform;
import com.redhat.aml.model.transform.TransactionTransform;

@Path("/accounts")
@Stateless
public class AccountEndpoint {

	@Inject
	AccountService accountService;
	
	@Inject
	TransactionService transactionService;
	
	
	// GET http://aml.opusdavei.org/aml-frontend-services/accounts/11223344/transactions
	@GET
	@Path("/{id}/transactions")
	@Produces(APPLICATION_JSON)
	public List<Transaction> getTransactions(@PathParam("id") final String id){
		List<TransactionDB> entities = transactionService.findTransactionsByAccountId(id);
		List<Transaction> transactions = new ArrayList<Transaction>(entities.size());
		for(TransactionDB entity : entities){
			transactions.add(TransactionTransform.transform(entity));
		}
		
		return transactions;
	}
	
	// POST http://aml.opusdavei.org/aml-frontend-services/accounts/
	// With body: {"accountNo": "2ead40ae-21d6-4786-9843-5f9c21a90c29", "accountType": "Savings", "age": 3, "city": "New York", "firstName": "Elmo", "lastName": "Monster", "marritialStatus": "s", "occupation": "Student", "sex": "m", "state": "NY", "street": "123 Sesame Street", "zipCode": "10001", "phoneNumber" : "7045551234"}
	@POST
	@Consumes(APPLICATION_JSON)
	public Response create(final Account account) {
		accountService.createAccount(AccountTransform.transform(account));
		return Response.created(UriBuilder.fromResource(AccountEndpoint.class).path(String.valueOf(account.getAccountNo())).build()).build();
	}

	// GET http://aml.opusdavei.org/aml-frontend-services/accounts/11223344
	@GET
	@Path("/{id}")
	@Produces(APPLICATION_JSON)
	public Response findById(@PathParam("id") final String id) {
		AccountDB entity = accountService.findAccount(id);
		
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		Account account = AccountTransform.transform(entity);
		return Response.ok(account).build();
	}
	
	// GET http://aml.opusdavei.org/aml-frontend-services/accounts/
	@GET
	@Produces(APPLICATION_JSON)
	public List<Account> listAll(
			@QueryParam("start") final Integer startPosition,
			@QueryParam("max") final Integer maxResult) {
		final List<AccountDB> entities = accountService.findAllAccounts();
		
		final List<Account> accounts = new ArrayList<Account>(entities.size());
		for(AccountDB e : entities){
			accounts.add(AccountTransform.transform(e));
		}
		
		return accounts;
	}

	// DELETE http://aml.opusdavei.org/aml-frontend-services/accounts/11223344
	@DELETE
	@Path("/{id}")
	public Response deleteById(@PathParam("id") final String id) {
		accountService.removeAccount(id);
		return Response.noContent().build();
	}

}
