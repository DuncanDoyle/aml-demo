package com.redhat.aml.restservices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import org.kie.api.runtime.KieSession;

import com.redhat.aml.model.Account;
import com.redhat.aml.model.Transaction;
import com.redhat.aml.model.TransactionDB;
import com.redhat.aml.model.service.AccountService;
import com.redhat.aml.model.service.TransactionService;
import com.redhat.aml.model.transform.AccountTransform;
import com.redhat.aml.model.transform.TransactionTransform;

@Path("/transactions")
@Stateless
public class TransactionEndpoint {
	
	private static final String TRANSACTION_PROCESS = "antimoneyLaundering.amlProcess";

	@Inject
	TransactionService transactionService;
	
	@Inject
	AccountService accountService;
	
	@Inject
	KieSession kieSession;

	// POST http://aml.opusdavei.org/aml-frontend-services/transactions
	// With Body: {"transactionid": 1, "accountNo": "c5d67bf6-d1c7-4d2a-be4a-2bb84be42326", "amount": 1000, "transactiontype": "Deposit", "fromZip": "Hooper's Store", "toZip": "NY", "ipaddress": "192.168.0.1", "devicelocation": "", "country": "US", "state": "NY", "datetime": 1438747200000, "transactionScore": "20.0", "note": ""}
	@POST
	@Consumes("application/json")
	public Response create(final Transaction transaction) {
		transaction.setNote(SAR_TEMPLATE.replace("_accountNo_", transaction.getAccountNo()));
		TransactionDB entity = TransactionTransform.transform(transaction);
		transactionService.createTransaction(entity);
		
		Account account = AccountTransform.transform(accountService.findAccount(transaction.getAccountNo()));
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("transaction", transaction);
		params.put("account", account);
		
		kieSession.startProcess(TRANSACTION_PROCESS, params);

		return Response.created(
				UriBuilder.fromResource(TransactionEndpoint.class)
						.path(String.valueOf(transaction.getTransactionid()))
						.build()).build();
	}

	// GET http://aml.opusdavei.org/aml-frontend-services/transactions/11223344
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public Response findById(@PathParam("id") final Integer id) {
		TransactionDB entity = transactionService.findTransaction(id);

		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		Transaction transaction = TransactionTransform.transform(entity);
		return Response.ok(transaction).build();
	}

	// GET http://aml.opusdavei.org/aml-frontend-services/transactions
	@GET
	@Produces("application/json")
	public List<Transaction> listAll(
			@QueryParam("start") final Integer startPosition,
			@QueryParam("max") final Integer maxResult) {
		
		final List<TransactionDB> entities = transactionService.findAllTransactions();
		final List<Transaction> transactions = new ArrayList<Transaction>(entities.size());
		for(TransactionDB e: entities){
			transactions.add(TransactionTransform.transform(e));
		}
		return transactions;
	}

	// DELETE http://aml.opusdavei.org/aml-frontend-services/transactions/11223344
	@DELETE
	@Path("/{id}")
	public Response deleteById(@PathParam("id") final Integer id) {
		transactionService.removeTransaction(id);
		return Response.noContent().build();
	}
	
	//_accountNo_
	private static final String SAR_TEMPLATE = 
			"A SAR will be filed relative to the activity in account #_accountNo_ for suspicious transfers and cash withdrawal transactions.\n"
		+   "Investigative Report\n"
		+	"This investigation, red alert #10, was raised in the month of August due to a referral from branch 11223. This investigation reviewed the customer's activity between Jan 1 2015 and Aug 30 2015. During this review period, eleven (11) suspicious transactions aggregating $42265 consisting of (2) suspicious incoming wire transfers aggregating $42265 consisting of 1) $22,300.00 on 8/27/08, 2) $19,965.00 on 9/15/08.\n"
		+	"Also, there were (2) suspicious cash withdrawals aggregating $76,040.00 as follows chronologically: 1) $16,000.00 check cashed on 09/03/08 at Branch 808, 2) $8,400 check cashed on 09/19/08 at 808,\n"
		+	"Often, the timing between deposits and withdrawals is in consecutive day or near consecutive days that given the appearance of structuring to avoid the filing requirements. It appears that the customer wrote checks to cash which he cashed an some of the withdrawals of those funds in amounts just below the federal reporting threshold, and did so with consecutive or near consecutive day transactions. It should also be noted the withdrawals of cash occurred shortly after the wire transfer deposits and in amounts nearly identical to the deposit amounts.\n"
		+	"Public records confirm the customer's information via a Lexis Nexis search or google search. No information was discovered related to the customer's occupation. Internet searches reveal that the customer is the owner of Juan Jewelry and Repair but no definitive information was found to verify the existence of that business.\n"
		+	"Conclusion\n"
		+	"These factors are an indication of structuring. Finally, because the customer's employment is unknown, a reasonable explanation from an earning or business standpoint cannot be determined for why the customer Location ?? would have engaged in these transactions. For these reasons, a SAR is being filed.";

}
