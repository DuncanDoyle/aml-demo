package com.redhat.aml.restservices;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.redhat.aml.model.Account;
import com.redhat.aml.model.AccountDB;
import com.redhat.aml.model.service.AccountService;
import com.redhat.aml.model.transform.AccountTransform;

@Path("/kyc")
@Stateless
public class KnowYourCustomerEndpoint {

	@Inject
	AccountService accountService;
	
	// GET /aml-frontend-services/kyc/11223344/score
	@GET
	@Path("/{id}/score")
	@Produces(APPLICATION_JSON)
	public Response getScore(@PathParam("id") final String id){
		AccountDB entity = accountService.findAccount(id);
		
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		Account account = AccountTransform.transform(entity);
		
		//Very meaningful KYC fraud score generation...
		int score = account.getAccountNo().hashCode() % 100;
		
		return Response.ok(score).build();
	}
}
