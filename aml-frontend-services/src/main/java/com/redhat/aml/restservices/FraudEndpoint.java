package com.redhat.aml.restservices;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.redhat.aml.model.Account;
import com.redhat.aml.model.Transaction;
import com.redhat.aml.model.service.AccountService;
import com.redhat.aml.model.service.FraudService;
import com.redhat.aml.model.transform.AccountTransform;

@Path("/fraud")
@Stateless
public class FraudEndpoint {
	private static final Logger LOG = LoggerFactory.getLogger(FraudEndpoint.class);
	
	@Inject
	private FraudService fraudService;
	
	@Inject
	private AccountService accountService;

	// POST http://aml.opusdavei.org/aml-frontend-services/accounts/
	// With body: {"accountNo": "2ead40ae-21d6-4786-9843-5f9c21a90c29", "accountType": "Savings", "age": 3, "city": "New York", "firstName": "Elmo", "lastName": "Monster", "marritialStatus": "s", "occupation": "Student", "sex": "m", "state": "NY", "street": "123 Sesame Street", "zipCode": "10001"}
	@POST
	@Path("/{id}")
	@Consumes(APPLICATION_JSON)
	public Response create(@PathParam("id") final String accountId, final Transaction suspectTransaction) {
		LOG.info("Suspicious transaction " + suspectTransaction + " for account=" + accountId);
		Account account = AccountTransform.transform(accountService.findAccount(accountId));
		fraudService.suspicousTransaction(account, suspectTransaction);
		return Response.ok().build();
	}
	
	@GET
	@Path("/")
	public Response parse(@QueryParam("From") final String phoneNumber,@QueryParam("Body") final String msgBody) {
		LOG.info(msgBody + " from number=" + phoneNumber);
		Account account = AccountTransform.transform(accountService.findAccountByPhoneNumber(phoneNumber));
		fraudService.parse(account, msgBody);
		return Response.ok().build();
	}
}
