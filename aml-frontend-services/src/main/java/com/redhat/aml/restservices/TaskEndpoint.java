package com.redhat.aml.restservices;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.kie.api.task.model.Status.*;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.jbpm.services.task.audit.impl.model.BAMTaskSummaryImpl;
import org.kie.api.task.TaskService;
import org.kie.api.task.model.Status;
import org.kie.api.task.model.TaskSummary;

import com.redhat.aml.model.Transaction;
import com.redhat.aml.model.service.TaskAuditService;

@Path("/tasks")
@Stateless
public class TaskEndpoint {

	@Inject
	TaskAuditService taskAuditService;

	@Inject
	TaskService taskService;

	// GET http://aml.opusdavei.org/aml-frontend-services/tasks/bpmsAdmin/tasklist
	@GET
	@Path("/{uid}/tasklist")
	@Produces(APPLICATION_JSON)
	public List<TaskSummary> getTasksForUser(@PathParam("uid") final String uid) {
		return taskService.getTasksAssignedAsPotentialOwner(uid, null);
	}
	
	// GET http://aml.opusdavei.org/aml-frontend-services/tasks/3/taskcontent
	@GET
	@Path("/{tid}/taskcontent")
	@Produces(APPLICATION_JSON)
	public Map<String, Object> getTaskContent(@PathParam("tid") final Long tid){
		return taskService.getTaskContent(tid);
	}
	
	// POST http://aml.opusdavei.org/aml-frontend-services/tasks/3/bpmsAdmin/addnote
	// With Body: "This is a note"
	@POST
	@Path("/{tid}/{uid}/addnote")
	@Consumes(APPLICATION_JSON)
	public Response addNote(@PathParam("tid") final Long tid, @PathParam("uid") final String user, final String note){
		Transaction transaction = (Transaction)taskService.getTaskContent(tid).get("transaction_inputSAR");
		taskService.start(tid, user);
		
		Map<String, Object> taskParams = new HashMap<String, Object>();
		transaction.setNote(note);
		taskParams.put("transaction_outputSAR", transaction);
		taskService.complete(tid, user, taskParams);
		
		return Response.noContent().build();
	}

	// GET http://aml.opusdavei.org/aml-frontend-services/tasks/bpmsAdmin/summary
	@GET
	@Path("/{uid}/summary")
	@Produces(APPLICATION_JSON)
	public DashboardSummary getSummary(@PathParam("uid") final String uid) {
		List<BAMTaskSummaryImpl> bamTasks = taskAuditService.getTaskSummaries();

		List<TaskSummary> runtimeTasks = taskService
				.getTasksAssignedAsPotentialOwner(uid, null);
		

		return new DashboardSummary(getTotalTasks(bamTasks), getMyTasks(
				runtimeTasks), getUrgentTasks(runtimeTasks),
				getTotalCompleted(bamTasks, uid),
				getAverageTimeToCompletion(bamTasks));
	}
	
	@GET
	@Path("{uid}/history")
	@Produces(APPLICATION_JSON)
	public List<List<Object[]>> getHistory(@PathParam("uid") final String uid){
		return taskAuditService.getTaskHistory(uid);
		
	}

	private long getTotalTasks(List<BAMTaskSummaryImpl> bamTasks) {
		long count = 0;
		for (BAMTaskSummaryImpl task : bamTasks) {
			List<String> statuses = Arrays.asList(Created.toString(), Ready.toString(), Reserved.toString(), InProgress.toString(), Suspended.toString());
			if (statuses.contains(task.getStatus())){
				count++;
			}
		}
		return count;
	}

	private long getMyTasks(List<TaskSummary> runtimeTasks) {
		return runtimeTasks.size();		
	}

	private long getUrgentTasks(List<TaskSummary> runtimeTasks) {
		long twoDaysInMillis = 2 * 24 * 60 * 60 * 1000; // days * hours *
														// minutes * seconds *
														// millis
		Date twoDaysFromNow = new Date(System.currentTimeMillis()
				+ twoDaysInMillis);

		long count = 0;
		for (TaskSummary task : runtimeTasks) {
			if (task.getExpirationTime() == null
					|| task.getExpirationTime().after(twoDaysFromNow)) {
				count++;
			}
		}

		return count;

	}

	private long getTotalCompleted(List<BAMTaskSummaryImpl> bamTasks, String uid) {
		long count = 0;
		for (BAMTaskSummaryImpl task : bamTasks) {
			if (Status.Completed.toString().equals(task.getStatus())  && uid.equals(task.getUserId())) {
				count++;
			}
		}

		return count;
	}

	private String getAverageTimeToCompletion(List<BAMTaskSummaryImpl> bamTasks) {

		long totalDuration = 0;
		int count = 0;
		for (BAMTaskSummaryImpl task : bamTasks) {
			if (task.getEndDate() != null) {
				totalDuration += task.getEndDate().getTime()
						- getCreatedDate(task).getTime();
				count++;
			}
		}
		long averageTimeToCompletion = count == 0 ? 0 : totalDuration / count;
		long millisperhour = 1000 * 60 * 60;
		long millisperminute = 1000 * 60;

		long hours = averageTimeToCompletion / millisperhour;
		long minutes = (averageTimeToCompletion - hours) / millisperminute;

		return hours + "h " + minutes + "m";

	}

	private Date getCreatedDate(BAMTaskSummaryImpl task) {

		try {
			Field createdDate = BAMTaskSummaryImpl.class
					.getDeclaredField("createdDate");
			createdDate.setAccessible(true);
			return (Date) createdDate.get(task);
		} catch (NoSuchFieldException e) {
			throw new IllegalStateException(
					"could not retrieve createdDate for task summary. cannot calculate average",
					e);
		} catch (SecurityException e) {
			throw new IllegalStateException(
					"could not retrieve createdDate for task summary. cannot calculate average",
					e);
		} catch (IllegalArgumentException e) {
			throw new IllegalStateException(
					"could not retrieve createdDate for task summary. cannot calculate average",
					e);
		} catch (IllegalAccessException e) {
			throw new IllegalStateException(
					"could not retrieve createdDate for task summary. cannot calculate average",
					e);
		}

	}
	
	public class DashboardSummary {
		private long totalTasks; // total tasks *open* in the system
		private long myTasks; // total *open* tasks assigned to groups user is in
		private long urgentTasks; // myTasks due within the next 48 hours 
		private long totalCompleted; // total tasks completed by user
		private String avgTimeToCompletion;

		public DashboardSummary() {
		}

		public DashboardSummary(long totalTasks, long myTasks,
				long urgentTasks, long totalCompleted,
				String avgTimeToCompletion) {
			super();
			this.totalTasks = totalTasks;
			this.myTasks = myTasks;
			this.urgentTasks = urgentTasks;
			this.totalCompleted = totalCompleted;
			this.avgTimeToCompletion = avgTimeToCompletion;
		}

		public long getTotalTasks() {
			return totalTasks;
		}

		public void setTotalTasks(long totalTasks) {
			this.totalTasks = totalTasks;
		}

		public long getMyTasks() {
			return myTasks;
		}

		public void setMyTasks(long myTasks) {
			this.myTasks = myTasks;
		}

		public long getUrgentTasks() {
			return urgentTasks;
		}

		public void setUrgentTasks(long urgentTasks) {
			this.urgentTasks = urgentTasks;
		}

		public long getTotalCompleted() {
			return totalCompleted;
		}

		public void setTotalCompleted(long totalCompleted) {
			this.totalCompleted = totalCompleted;
		}

		public String getAvgTimeToCompletion() {
			return avgTimeToCompletion;
		}

		public void setAvgTimeToCompletion(String avgTimeToCompletion) {
			this.avgTimeToCompletion = avgTimeToCompletion;
		}

	}

}
