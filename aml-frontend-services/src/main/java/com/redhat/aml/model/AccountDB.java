package com.redhat.aml.model;

// Generated Aug 2, 2015 9:10:48 PM by Hibernate Tools 4.3.1

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Account
 */
@Entity
@Table(name = "accountdetails")
public class AccountDB implements java.io.Serializable {

	/** svuid */
	private static final long serialVersionUID = 5270563428161997236L;
	
	String accountNo;
	String accountType;
	Integer age;
	String city;
	String firstName;
	String lastName;
	String marritialStatus;
	String occupation;
	String sex;
	String state;
	String street;
	String zipCode;
	String phoneNumber;
	
	public AccountDB() {
	}
	
	public AccountDB(String accountNo, String accountType, Integer age,
			String city, String firstName, String lastName,
			String marritialStatus, String occupation, String sex,
			String state, String street, String zipCode, String phoneNumber) {
		super();
		this.accountNo = accountNo;
		this.accountType = accountType;
		this.age = age;
		this.city = city;
		this.firstName = firstName;
		this.lastName = lastName;
		this.marritialStatus = marritialStatus;
		this.occupation = occupation;
		this.sex = sex;
		this.state = state;
		this.street = street;
		this.zipCode = zipCode;
		this.phoneNumber = phoneNumber;
	}


	@Id
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMarritialStatus() {
		return marritialStatus;
	}
	public void setMarritialStatus(String marritialStatus) {
		this.marritialStatus = marritialStatus;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}
