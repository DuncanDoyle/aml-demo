package com.redhat.aml.model.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.kie.api.task.TaskService;
import org.kie.api.task.model.Status;
import org.kie.api.task.model.TaskSummary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.redhat.aml.model.Account;
import com.redhat.aml.model.Transaction;
import com.twilio.sdk.resource.instance.Message;

@Singleton
public class FraudService {
	private static final Logger LOG = LoggerFactory.getLogger(FraudService.class);
	
	@Inject
	private SmsService smsService;
	
	@Inject
	private TaskService taskService;
	
	public void suspicousTransaction(final Account account, final Transaction transaction) {
		Message message = smsService.sendMessage(account, "A suspicious transaction of " + transaction.getAmount() + " occured on your account, reply CONFIRM to authorize the transaction and DENY to halt processing");
		LOG.info("Sent message " + message.getSid());
	}
	
	public void parse(final Account account, String msg) {
		if ("confirm".equalsIgnoreCase(msg) || "c".equalsIgnoreCase(msg)) {
			this.confirm(account);
		} else if ("deny".equalsIgnoreCase(msg) || "d".equalsIgnoreCase(msg)) {
			this.deny(account);
		} else {
			smsService.sendMessage(account, "Unrecognized option, please either CONFIRM or DENY");
		}
	}

	private void confirm(final Account account) {
		Message message = respond(account, true);
		LOG.info("Sent message " + message.getSid());
	}
	
	private void deny(final Account account) {
		Message message = respond(account, false);
		LOG.info("Sent message " + message.getSid());
	}
	
	private Message respond(final Account account, boolean confirm) {
		String user = "SMS-" + account.getAccountNo();
		List<TaskSummary> taskSummaries = taskService.getTasksAssignedAsPotentialOwner(user, null);
		Message message = null;
		
		if (taskSummaries.isEmpty()) {
			message = smsService.sendMessage(account, "No transactions awaiting confirmation");
		} else {
			boolean messageSent = false;
			for (TaskSummary summary : taskSummaries) {
				Status taskStatus = summary.getStatus();
				if (Status.Ready.equals(taskStatus) || Status.Reserved.equals(taskStatus)) {
					try {
						taskService.claim(summary.getId(), user);
					} catch (Exception e) {
						LOG.trace("Could not claim " + summary, e);
					}
					
					try {
						taskService.start(summary.getId(), user);
					} catch (Exception e) {
						LOG.trace("Could not start " + summary, e);
					}
					
					try {
						Map<String, Object> parameters = new HashMap<String, Object>(1);
						parameters.put("confirm", confirm);
						taskService.complete(summary.getId(), user, parameters);
						message = smsService.sendMessage(account, "Thank you for your " + (confirm? "confirmation" : "denail"));
						messageSent = true;
					} catch (Exception e) {
						LOG.error("Could not complete " + summary, e);
					}
				}
				
			}
			if (!messageSent) {
				message = smsService.sendMessage(account, "No transactions awaiting confirmation");
			}
		}
		
		return message;
	}
}
