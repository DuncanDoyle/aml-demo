package com.redhat.aml.model.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.kie.api.KieBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.redhat.aml.model.Row;
import com.redhat.aml.model.RuleExecutionDB;
import com.redhat.aml.util.GitUtils;

@Singleton
public class RuleExecutionService {
	private static final Logger LOG = LoggerFactory.getLogger(RuleExecutionService.class);

	@PersistenceContext(unitName="aml-frontend-services")
	EntityManager em;
	
	public RuleExecutionDB createRuleExecution(
			Integer transactionId, String packageName, String ruleName) {
		RuleExecutionDB ruleExecution = new RuleExecutionDB(null, transactionId, packageName, ruleName);

		return createRuleExecution(ruleExecution);
	}

	public RuleExecutionDB createRuleExecution(RuleExecutionDB ruleExecution) {
		if(ruleExecution.getId() != null){
			ruleExecution.setId(null);
		}
		em.persist(ruleExecution);
		return ruleExecution;
	}
	
	public List<RuleExecutionDB> findRuleExecutionsByTransactionId(Integer transactionId){
		return em.createQuery("FROM RuleExecutionDB t WHERE t.transactionId = :transactionId", RuleExecutionDB.class).setParameter("transactionId", transactionId).getResultList();
	}
	
	public List<Row> getAllRows(Integer transactionId) {
		List<Row> allRules = null;
		try {
			allRules = this.getAllRules();
		} catch (GitAPIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this.get(transactionId, allRules);
	}
	
	
	//have mercy on me...
	private List<Row> getAllRules() throws GitAPIException, IOException {
		
		GitUtils gitUtils = new GitUtils();
		//gitUtils.
		String username = "bpmsAdmin";
		String password = "abcd1234!";
		String hostname = "localhost";
		String port = "8001";
		String repository = "aml";
		String gitUrl = "ssh://" + username + "@"+hostname+":"+port+"/" + repository;
		
		gitUtils.cloneRepo(gitUrl, password);
		File decisionTable = gitUtils.getDecisionTable();
		
		List<Row> rows = new ArrayList<Row>();
		BufferedReader br = new BufferedReader(new FileReader(decisionTable));
		String line = "";
		int lineNumber = 0;
		
		//Header is 173 lines long
		for (int i = 0; !line.contains("valueNumeric"); i++) {
			line = br.readLine();
			lineNumber++;
		}
		
		while (line != null){
			rows.add(getRow(line, lineNumber, br));
			line = br.readLine();
			lineNumber++;
		}

		
		br.close();
		return rows;
	}
	
	private Row getRow(String l, int lN, BufferedReader br) throws IOException {
		LOG.info("Starting at " + l);
		Row row = new Row();
		
		//row number
		String line = l;
		int lineNumber = lN;
		String[] tokens = line.split("<|>");
		int rowNumber = Integer.parseInt(tokens[2]);
		LOG.info("Parsed row number " + rowNumber + " from " + line);
		row.setName("Row " + rowNumber + " CustomerProfileGDT");
		
		
		//skip
		for (int i = 1; i < 20; i++) {
			br.readLine();
			lineNumber++;
		}
		
		//zipcode
		line = br.readLine();
		lineNumber++;
		tokens = line.split("<|>");
		String zipCode = tokens[2];
		LOG.info("Parsed zipcode " + zipCode+ " from " + line);
		row.setZipCode(zipCode);
		
		
		
		//skip
		for (int i = 1; i < 5; i++) {
			br.readLine();
			lineNumber++;
		}
		
		//occupation
		line = br.readLine();
		lineNumber++;
		tokens = line.split("<|>");
		String occupation = tokens[2];
		LOG.info("Parsed occupation " + occupation+ " from " + line);
		row.setOccupation(occupation);
		
		
		
		
		//skip
		for (int i = 1; i < 5; i++) {
			br.readLine();
			lineNumber++;
		}
		
		//limit
		line = br.readLine();
		lineNumber++;
		tokens = line.split("<|>");
		String limit = tokens[2];
		LOG.info("Parsed limit " + limit + " from " + line);
		row.setLimit(Integer.parseInt(limit));
		
		
		
		//skip
		for (int i = 1; i < 5; i++) {
			br.readLine();
			lineNumber++;
		}
		
		//type
		line = br.readLine();
		lineNumber++;
		tokens = line.split("<|>");
		String type = tokens[2];
		LOG.info("Parsed type " + type+ " from " + line);
		row.setTransactionType(type);
		
		
		
		//skip
		for (int i = 1; i < 5; i++) {
			br.readLine();
			lineNumber++;
		}
		
		//score
		line = br.readLine();
		lineNumber++;
		tokens = line.split("<|>");
		String score = tokens[2];
		LOG.info("Parsed score " + score+ " from " + line);
		row.setTransactionScore(score);
		
		
		//skip
		for (int i = 0; i < 6; i++) {
			br.readLine();
			lineNumber++;
		}
		
		return row;
	}
	
	private List<Row> get(Integer transactionId, List<Row> allRules){
		List<RuleExecutionDB> ruleExecutionDBs = this.findRuleExecutionsByTransactionId(transactionId);
		List<Row> toReturn = new ArrayList<Row>(ruleExecutionDBs.size());
		for (Row rule : allRules) {
			rule.setFired(this.contains(ruleExecutionDBs, rule));
			toReturn.add(rule);
		}
		return toReturn;
	}
	
	private boolean contains(List<RuleExecutionDB> ruleExecutionDBs, Row rule) {
		String name = rule.getName();
		boolean found = false;
		for (RuleExecutionDB ruleExecutionDB : ruleExecutionDBs) {
			if (name.equals(ruleExecutionDB.getRuleName())){
				found = true;
				break;
			}
		}
		return found;
	}

}
