package com.redhat.aml.model;

// Generated Aug 2, 2015 9:10:48 PM by Hibernate Tools 4.3.1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Transaction 
 */
@Entity
@Table(name = "transaction")
public class TransactionDB implements java.io.Serializable {

	/** svuid */
	private static final long serialVersionUID = -1723072699124282844L;
	
	private Integer transactionid;
	private String accountNo;
	private Long amount;
	private String transactiontype;
	private String fromloc;
	private String toloc;
	private String ipaddress;
	private String devicelocation;
	private String country;
	private String state;
	private Date datetime;
	private String note;

	public TransactionDB() {
	}

	public TransactionDB(Integer transactionid, String accountNo, Long amount,
			String transactiontype, String fromloc, String toloc,
			String ipaddress, String devicelocation, String country,
			String state, Date datetime, String note) {
		super();
		this.transactionid = transactionid;
		this.accountNo = accountNo;
		this.amount = amount;
		this.transactiontype = transactiontype;
		this.fromloc = fromloc;
		this.toloc = toloc;
		this.ipaddress = ipaddress;
		this.devicelocation = devicelocation;
		this.country = country;
		this.state = state;
		this.datetime = datetime;
		this.note = note;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "transactionid", unique = true, nullable = false)
	public Integer getTransactionid() {
		return this.transactionid;
	}

	public void setTransactionid(Integer transactionid) {
		this.transactionid = transactionid;
	}

	@Column(name = "accountNo", nullable = false)
	public String getAccountNo() {
		return this.accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	@Column(name = "amount", nullable = false)
	public Long getAmount() {
		return this.amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	@Column(name = "transactiontype", nullable = false, length = 100)
	public String getTransactiontype() {
		return this.transactiontype;
	}

	public void setTransactiontype(String transactiontype) {
		this.transactiontype = transactiontype;
	}

	@Column(name = "fromloc", nullable = false, length = 100)
	public String getFromloc() {
		return this.fromloc;
	}

	public void setFromloc(String fromloc) {
		this.fromloc = fromloc;
	}

	@Column(name = "toloc", nullable = false, length = 100)
	public String getToloc() {
		return this.toloc;
	}

	public void setToloc(String toloc) {
		this.toloc = toloc;
	}

	@Column(name = "ipaddress", nullable = false, length = 100)
	public String getIpaddress() {
		return this.ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	@Column(name = "devicelocation", nullable = false, length = 100)
	public String getDevicelocation() {
		return this.devicelocation;
	}

	public void setDevicelocation(String devicelocation) {
		this.devicelocation = devicelocation;
	}

	@Column(name = "country", nullable = false, length = 100)
	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name = "state", nullable = false, length = 100)
	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datetime", length = 19)
	public Date getDatetime() {
		return this.datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}

	@Lob
	@Column(name = "note", columnDefinition = "text")
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
	

}
