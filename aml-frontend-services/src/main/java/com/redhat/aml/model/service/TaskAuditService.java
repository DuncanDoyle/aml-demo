package com.redhat.aml.model.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.jbpm.services.task.audit.impl.model.BAMTaskSummaryImpl;

@Singleton
public class TaskAuditService {
	
	@PersistenceContext(unitName="bpms-services")
	EntityManager em;

	public List<BAMTaskSummaryImpl> getTaskSummaries(){
		TypedQuery<BAMTaskSummaryImpl> query = em.createQuery(
				"FROM BAMTaskSummaryImpl", BAMTaskSummaryImpl.class);
		List<BAMTaskSummaryImpl> bamTasks = query.getResultList();
		
		return bamTasks;
	}
	
	public List<List<Object[]>> getTaskHistory(final String userId){
		List<List<Object[]>> results = new ArrayList<List<Object[]>>();
		Query createdDates = em.createNativeQuery("select createdDate, count(*) from bamtasksummary where userId = :userId group by DATE(createdDate)").setParameter("userId", userId);
		Query endDates = em.createNativeQuery("select endDate, count(*) from bamtasksummary where userId = :userId and endDate is not null group by DATE(endDate)").setParameter("userId", userId);
		
		results.add(createdDates.getResultList());
		results.add(endDates.getResultList());
		return results;
	}

}
