package com.redhat.aml.model.service;

import com.redhat.aml.model.*;

import javax.inject.Singleton;
import javax.persistence.*;

import java.util.Date;
import java.util.List;

@Singleton
public class TransactionService {

	@PersistenceContext(unitName="aml-frontend-services")
	EntityManager em;

	public TransactionDB createTransaction(
			String firstname, String lastname, String accountNo, Long amount,
			String transactiontype, String fromloc, String toloc,
			String ipaddress, String devicelocation, String country,
			String state, Date datetime, String note) {
		TransactionDB transaction = new TransactionDB(null, accountNo, amount, transactiontype, fromloc, toloc,
				ipaddress, devicelocation, country, state, datetime, note);

		return createTransaction(transaction);
	}

	public TransactionDB createTransaction(TransactionDB transaction) {
		if(transaction.getTransactionid() != null){
			transaction.setTransactionid(null);
		}
		em.persist(transaction);
		return transaction;
	}

	public void removeTransaction(int id) {
		TransactionDB acc = findTransaction(id);
		if (acc != null) {
			em.remove(acc);
		}
	}

	public TransactionDB findTransaction(int id) {
		return em.find(TransactionDB.class, id);
	}
	
	public List<TransactionDB> findTransactionsByAccountId(String accountId){
		return em.createQuery("FROM TransactionDB t WHERE t.accountNo = :accountId", TransactionDB.class).setParameter("accountId", accountId).getResultList();
	}

	public List<TransactionDB> findAllTransactions() {
		TypedQuery<TransactionDB> query = em.createQuery("FROM TransactionDB",
				TransactionDB.class);
		return query.getResultList();
	}
}