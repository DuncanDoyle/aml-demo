package com.redhat.aml.model.transform;

import com.redhat.aml.model.Account;
import com.redhat.aml.model.AccountDB;

public class AccountTransform {

	public static Account transform(AccountDB input) {
		Account output = new Account(input.getAccountNo(),
				input.getAccountType(), input.getAge(), input.getCity(),
				input.getFirstName(), input.getLastName(),
				input.getMarritialStatus(), input.getOccupation(),
				input.getSex(), input.getState(), input.getStreet(),
				input.getZipCode(), input.getPhoneNumber());

		return output;
	}

	public static AccountDB transform(Account input) {
		AccountDB output = new AccountDB(input.getAccountNo(),
				input.getAccountType(), input.getAge(), input.getCity(),
				input.getFirstName(), input.getLastName(),
				input.getMarritialStatus(), input.getOccupation(),
				input.getSex(), input.getState(), input.getStreet(),
				input.getZipCode(), input.getPhoneNumber());

		return output;
	}

}
