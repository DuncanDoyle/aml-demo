package com.redhat.aml.model.transform;

import com.redhat.aml.model.RuleExecution;
import com.redhat.aml.model.RuleExecutionDB;


public class RuleExecutionTransform {

	public static RuleExecution transform(RuleExecutionDB input) {
		RuleExecution output = new RuleExecution(input.getId(), input.getTransactionId(), input.getPackageName(), input.getRuleName());

		return output;
	}

	public static RuleExecutionDB transform(RuleExecution input) {
		RuleExecutionDB output = new RuleExecutionDB(input.getId(), input.getTransactionId(), input.getPackageName(), input.getRuleName());

		return output;
	}

}
