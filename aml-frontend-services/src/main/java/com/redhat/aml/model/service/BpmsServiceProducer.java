package com.redhat.aml.model.service;

import java.net.MalformedURLException;
import java.net.URL;

import javax.enterprise.inject.Produces;

import org.kie.api.runtime.KieSession;
import org.kie.api.task.TaskService;
import org.kie.remote.client.api.RemoteRestRuntimeEngineFactory;
import org.kie.remote.client.api.RemoteRuntimeEngineFactory;
import org.kie.remote.client.api.exception.InsufficientInfoToBuildException;

public class BpmsServiceProducer {
	
	private final RemoteRestRuntimeEngineFactory factory;

	public BpmsServiceProducer() throws InsufficientInfoToBuildException,
			MalformedURLException {
		factory = RemoteRuntimeEngineFactory.newRestBuilder()
				.addUrl(new URL("http://localhost:8080/business-central"))
				.addDeploymentId("com.redhat:antimoneyLaundering:1.0")
				.addUserName("bpmsAdmin").addPassword("abcd1234!")
				.buildFactory();
	}
	
	
	@Produces
	public TaskService taskService(){
		return factory.newRuntimeEngine().getTaskService();
	}
	
	@Produces KieSession kieSession(){
		return factory.newRuntimeEngine().getKieSession();
	}
	
}
