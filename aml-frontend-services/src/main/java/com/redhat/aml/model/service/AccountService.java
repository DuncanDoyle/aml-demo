package com.redhat.aml.model.service;

import java.util.List;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.redhat.aml.model.AccountDB;

@Singleton
public class AccountService {
	private static final Logger LOG = LoggerFactory.getLogger(AccountService.class);

	@PersistenceContext(unitName="aml-frontend-services")
	EntityManager em;

	public AccountDB createAccount(String accountNo, String accountType,
			Integer age, String city, String firstName, String lastName,
			String marritialStatus, String occupation, String sex,
			String state, String street, String zipCode, String phoneNumber) {
		AccountDB acc = new AccountDB(accountNo, accountType, age, city,
				firstName, lastName, marritialStatus, occupation, sex, state,
				street, zipCode, phoneNumber);

		return createAccount(acc);
	}

	public AccountDB createAccount(AccountDB account) {
		em.persist(account);
		return account;
	}

	public void removeAccount(String id) {
		AccountDB acc = findAccount(id);
		if (acc != null) {
			em.remove(acc);
		}
	}

	public AccountDB findAccount(String id) {
		return em.find(AccountDB.class, id);
	}

	public List<AccountDB> findAllAccounts() {
		TypedQuery<AccountDB> query = em.createQuery("FROM AccountDB",
				AccountDB.class);
		return query.getResultList();
	}
	
	public AccountDB findAccountByPhoneNumber(String number) {
		if (number.startsWith("+1")) { //trim US numbers
			number = number.substring(2);
		}
		LOG.info("Searching for account associated with " + number);
		return (AccountDB) em.createQuery("FROM AccountDB where phoneNumber = :phoneNumber").setParameter("phoneNumber", number).getSingleResult();
		
	}
}