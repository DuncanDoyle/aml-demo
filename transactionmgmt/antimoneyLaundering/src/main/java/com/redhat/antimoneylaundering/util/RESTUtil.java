package com.redhat.antimoneylaundering.util;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RESTUtil {
	private static final Logger LOG = LoggerFactory.getLogger(RESTUtil.class);
	
	private static final String HOSTNAME = System.getProperty("aml.hostname", "localhost");
	private static final Integer PORT = Integer.parseInt(System.getProperty("aml.port", "8080"));
	
	public static void POST(String resource, Object payload) {
		ObjectMapper mapper = new ObjectMapper();
		HttpEntity httpEntity = null;
		try {
			httpEntity = new StringEntity(
					mapper.writeValueAsString(payload),
					ContentType.create("application/json"));
		} catch (JsonGenerationException e) {
			LOG.error("Error creating JSON", e);
		} catch (JsonMappingException e) {
			LOG.error("Error creating JSON", e);
		} catch (IOException e) {
			LOG.error("Error creating JSON", e);
		}

		HttpHost target = new HttpHost(HOSTNAME, PORT);
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(resource);
		httpPost.setEntity(httpEntity);

		try {
			LOG.info("HTTP POST = " + httpPost);
			CloseableHttpResponse response = httpclient.execute(target, httpPost);
			LOG.info("Response = " + response);
			response.close();
		} catch (ClientProtocolException e) {
			LOG.error("Error performing POST", e);
		} catch (IOException e) {
			LOG.error("Error performing POST", e);
		}
	}
}
