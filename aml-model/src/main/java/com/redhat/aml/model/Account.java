package com.redhat.aml.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;

@XmlRootElement(name="account")
@XmlAccessorType(XmlAccessType.FIELD)
public class Account implements Serializable{
	
	/** svuid */
	private static final long serialVersionUID = -4538233841890072959L;

	@XmlElement
	@XmlSchemaType(name="string")
	private String accountNo;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String accountType;
	
	@XmlElement
	@XmlSchemaType(name="int")
	private Integer age;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String city;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String firstName;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String lastName;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String marritialStatus;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String occupation;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String sex;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String state;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String street;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String zipCode;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String phoneNumber;

	public Account() {
	}

	public Account(String accountNo, String accountType, Integer age,
			String city, String firstName, String lastName,
			String marritialStatus, String occupation, String sex,
			String state, String street, String zipCode, String phoneNumber) {
		super();
		this.accountNo = accountNo;
		this.accountType = accountType;
		this.age = age;
		this.city = city;
		this.firstName = firstName;
		this.lastName = lastName;
		this.marritialStatus = marritialStatus;
		this.occupation = occupation;
		this.sex = sex;
		this.state = state;
		this.street = street;
		this.zipCode = zipCode;
		this.phoneNumber = phoneNumber;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMarritialStatus() {
		return marritialStatus;
	}

	public void setMarritialStatus(String marritialStatus) {
		this.marritialStatus = marritialStatus;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "Account [accountNo=" + accountNo + ", accountType="
				+ accountType + ", age=" + age + ", city=" + city
				+ ", firstName=" + firstName + ", lastName=" + lastName
				+ ", marritialStatus=" + marritialStatus + ", occupation="
				+ occupation + ", sex=" + sex + ", state=" + state
				+ ", street=" + street + ", zipCode=" + zipCode
				+ ", phoneNumber=" + phoneNumber +"]";
	}
	
	

}
