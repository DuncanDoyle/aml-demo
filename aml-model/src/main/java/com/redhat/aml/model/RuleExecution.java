package com.redhat.aml.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;

@XmlRootElement(name="ruleexecution")
@XmlAccessorType(XmlAccessType.FIELD)
public class RuleExecution implements java.io.Serializable {
	
	private static final long serialVersionUID = -8043427033197831313L;

	@XmlElement
	@XmlSchemaType(name="int")
	private Integer id;
	
	@XmlElement
	@XmlSchemaType(name="int")
	private Integer transactionId;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String packageName;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String ruleName;
	
	public RuleExecution() {
		super();
	}

	public RuleExecution(Integer id, Integer transactionId,
			String packageName, String ruleName) {
		super();
		this.id = id;
		this.transactionId = transactionId;
		this.packageName = packageName;
		this.ruleName = ruleName;
	}

	
	public Integer getId() {
		return this.id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

}
