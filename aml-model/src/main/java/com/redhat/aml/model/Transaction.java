package com.redhat.aml.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;

@XmlRootElement(name="transaction")
@XmlAccessorType(XmlAccessType.FIELD)
public class Transaction implements Serializable{
	
	/** svuid */
	private static final long serialVersionUID = -7195762319803936298L;

	@XmlElement
	@XmlSchemaType(name="int")
	private Integer transactionid;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String accountNo;
	
	@XmlElement
	@XmlSchemaType(name="long")
	private Long amount;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String transactiontype;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String fromZip;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String toZip;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String ipaddress;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String devicelocation;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String country;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String state;
	
	@XmlElement
	@XmlSchemaType(name="date")
	private Date datetime;
	
	@XmlElement
	@XmlSchemaType(name="double")
	private Double transactionScore = 0.0;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String note;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String process;

	public Transaction() {
	}

	public Transaction(Integer transactionid, String accountNo, Long amount,
			String transactiontype, String fromZip, String toZip,
			String ipaddress, String devicelocation, String country,
			String state, Date datetime, String note) {
		super();
		this.transactionid = transactionid;
		this.accountNo = accountNo;
		this.amount = amount;
		this.transactiontype = transactiontype;
		this.fromZip = fromZip;
		this.toZip = toZip;
		this.ipaddress = ipaddress;
		this.devicelocation = devicelocation;
		this.country = country;
		this.state = state;
		this.datetime = datetime;
		this.note = note;
	}

	public Integer getTransactionid() {
		return transactionid;
	}

	public void setTransactionid(Integer transactionid) {
		this.transactionid = transactionid;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getTransactiontype() {
		return transactiontype;
	}

	public void setTransactiontype(String transactiontype) {
		this.transactiontype = transactiontype;
	}

	public String getFromZip() {
		return fromZip;
	}

	public void setFromZip(String fromZip) {
		this.fromZip = fromZip;
	}

	public String getToZip() {
		return toZip;
	}

	public void setToZip(String toZip) {
		this.toZip = toZip;
	}

	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	public String getDevicelocation() {
		return devicelocation;
	}

	public void setDevicelocation(String devicelocation) {
		this.devicelocation = devicelocation;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getDatetime() {
		return datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}

	public Double getTransactionScore() {
		return transactionScore;
	}

	public void setTransactionScore(Double transactionScore) {
		this.transactionScore = transactionScore;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}
	
	@Override
	public String toString() {
		return "Transaction [transactionid=" + transactionid + ", accountNo="
				+ accountNo + ", amount=" + amount + ", transactiontype="
				+ transactiontype + ", fromZip=" + fromZip + ", toZip=" + toZip
				+ ", ipaddress=" + ipaddress + ", devicelocation="
				+ devicelocation + ", country=" + country + ", state=" + state
				+ ", datetime=" + datetime + ", transactionScore="
				+ transactionScore + ", note=" + note + ", process=" + process
				+ "]";
	}
	
}