package com.redhat.aml.model;


public class CustomerProfile {
	private String zipCode;
	private String transactionType; //Deposit,Withdrawal
	private String occupation;
	private Double totalSpent;
	private Integer numberOfTransactions;
	private Double average;
	private Double minimum;
	private Double maximum;
	private Double standardDeviation;
	private Integer lowPenalty;
	private Integer highPenalty;
	
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public Double getTotalSpent() {
		return totalSpent;
	}
	public void setTotalSpent(Double totalSpent) {
		this.totalSpent = totalSpent;
	}
	public Integer getNumberOfTransactions() {
		return numberOfTransactions;
	}
	public void setNumberOfTransactions(Integer numberOfTransactions) {
		this.numberOfTransactions = numberOfTransactions;
	}
	public Double getAverage() {
		return average;
	}
	public void setAverage(Double average) {
		this.average = average;
	}
	public Double getMinimum() {
		return minimum;
	}
	public void setMinimum(Double minimum) {
		this.minimum = minimum;
	}
	public Double getMaximum() {
		return maximum;
	}
	public void setMaximum(Double maximum) {
		this.maximum = maximum;
	}
	public Double getStandardDeviation() {
		return standardDeviation;
	}
	public void setStandardDeviation(Double standardDeviation) {
		this.standardDeviation = standardDeviation;
	}
	public Integer getLowPenalty() {
		return lowPenalty;
	}
	public void setLowPenalty(Integer lowPenalty) {
		this.lowPenalty = lowPenalty;
	}
	public Integer getHighPenalty() {
		return highPenalty;
	}
	public void setHighPenalty(Integer highPenalty) {
		this.highPenalty = highPenalty;
	}
	@Override
	public String toString() {
		return "CustomerProfile [zipCode=" + zipCode + ", transactionType="
				+ transactionType + ", occupation=" + occupation
				+ ", totalSpent=" + totalSpent + ", numberOfTransactions="
				+ numberOfTransactions + ", average=" + average + ", minimum="
				+ minimum + ", maximum=" + maximum + "]";
	}
}